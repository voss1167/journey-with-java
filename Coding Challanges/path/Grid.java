import java.io.*;
import java.util.*;

class Grid{
  private int X;
  private int Y;
  private ArrayList<ArrayList<Cell>> GRID;

  Grid(){
    this(200, 300);
  }

  Grid(int x, int y){
    this.X = x;
    this.Y = y;
    this.GRID = new ArrayList<ArrayList<Cell>>();
    ArrayList<Cell> tempRow = new ArrayList<Cell>();
    Cell tempCell = new Cell();
    for(int i = 0; i < y; i++){
      tempRow = new ArrayList<Cell>();
      for(int j = 0; j < x; j++){
        tempCell = new Cell();
        tempRow.add(tempCell);
      }
      this.GRID.add(tempRow);
    }
  }

  public int getX(){ return this.X; }
  public int getY(){ return this.Y; }

  public void print(){
    for(int i = 0; i < this.GRID.size(); i++){
      System.out.println(this.GRID.get(i));
    }
  }
}

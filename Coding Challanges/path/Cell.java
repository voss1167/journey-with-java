class Cell{
  private boolean FILL;
  private boolean PATH;

  Cell(){ this(false, false); }

  Cell(boolean path, boolean fill){
    this.PATH = path;
    this.FILL = fill;
  }

  public boolean getFill(){ return this.FILL; }
  public boolean getPath(){ return this.PATH; }

  public void setFill(boolean fill){ this.FILL = fill;}
  public void setPath(boolean path){ this.PATH = path;}

  public String toString(){
    return String.valueOf(this.FILL) + ':' + String.valueOf(this.PATH);
  }
}

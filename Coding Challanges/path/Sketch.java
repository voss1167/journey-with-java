import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;

class Sketch extends JPanel{
  // Dimentions
  private static int x = 150; // Num of boxes in x
  private static int y = 100; // Num of boxes in y
  private static int p = 20; // Pixels per box

  private static int xP = x * p;
  private static int yP = y * p;

  public static void main(String[] args) {
    // Set up window
    JFrame f = new JFrame("Path Finding");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(new Sketch());
    f.setSize(xP, yP);
    f.setLocation(550, 25);
    f.setVisible(true);

    // Create grid
    Grid grid = new Grid(x, y);

  }

  public void paint(Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    g2.setColor(new Color(0f, 0f, 0f)); // Set grid color

    this.draw(g2);
  }

  public void drawGrid(Graphics g2){
    for(int i = this.p; i < xP; i+=this.p)
      g2.drawLine(i, 0, i, yP);
    for(int i = this.p; i < yP; i+=this.p)
      g2.drawLine(0, i, xP, i);}

  public void draw(Graphics g2){
    drawGrid(g2);

    this.repaint();
  }
}

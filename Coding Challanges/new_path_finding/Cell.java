class Cell{
  protected boolean FILL;

  Cell(){
    this.FILL = false;
  }

  Cell(boolean fill){
    this.FILL = fill;
  }

  public void setFill(boolean fill){
    this.FILL = fill;
  }

  public boolean getFill(){
    return this.FILL;
  }

  public String toString(){
    return String.valueOf(this.FILL);
  }
}

class CellPath extends Cell{
  private boolean STATUS;

  CellPath(){
    this(false);
  }

  CellPath(boolean status){
    super();
    this.STATUS = status;
  }

  CellPath(boolean status, boolean fill){
    super(fill);
    this.STATUS = status;
  }

  public void setStatus(boolean status){
    this.STATUS = status;
  }

  public boolean getStatus(){
    return this.STATUS;
  }
}

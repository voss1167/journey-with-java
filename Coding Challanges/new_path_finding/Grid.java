import java.io.*;
import java.util.*;

class Grid{
  private ArrayList<ArrayList<CellPath>> GRID;

  Grid(){
    this(400, 600);
  }

  Grid(int x, int y){
    this.GRID = new ArrayList<ArrayList<CellPath>>();
    ArrayList<CellPath> tempArray  = new ArrayList<CellPath>();
    CellPath tempCell = new CellPath();
    for(int i = 0; i < y; i++){
      tempArray = new ArrayList<CellPath>();
      for(int j = 0; j < x; j++){
        tempCell = new CellPath();
        tempArray.add(tempCell);
      }
      this.GRID.add(tempArray);
    }
  }

  public CellPath getCellPath(int x, int y){
    return this.GRID.get(y).get(x);
  }

  public void setFill(int x, int y, boolean fill){
    getCellPath(x, y).setFill(fill);
  }

  public ArrayList<CellPath> getRow(int y){
    return this.GRID.get(y);
  }

  public setFill(int x, int y, boolean fill){
    CellPath tempCell = this.C
    this.getRow(y).set(x, tempCell);
  }

  public

  public boolean getFill(int x, int y){
    return getCellPath(x, y).getFill();
  }

  public boolean getStatus(int x, int y){
    return getCellPath(x, y).getStatus();
  }

  public void printGrid(){
    for(int i = 0; i < this.GRID.size(); i++){
      System.out.println(getRow(i));
    }
  }
}

class GridPath extends Path{
  GridPath(){
    super();
  }
}

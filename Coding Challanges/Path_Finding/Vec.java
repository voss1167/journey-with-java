class Vec{
  private int X;
  private int Y;
  private int H;

  Vec(){
    this.X = 0;
    this.Y = 0;
  }

  Vec(int x, int y){
    this.X = x;
    this.Y = y;
  }

  public void setX(int x){ this.X = x; }
  public void setY(int y){ this.Y = y; }

  public int getX(){ return this.X; }
  public int getY(){ return this.Y; }
}

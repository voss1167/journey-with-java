import java.awt.Canvas;
import java.awt.Graphics;
import javax.swing.JFrame;

import java.io.*;
import java.util.*;

class Grid{
  private int W, H, R, C;
  private ArrayList<ArrayList> GRID;

  Grid(){
    this(400, 600);
  }

  Grid(int w, int h){
    this(w, h, w / 10, h / 10);
  }

  Grid(int w, int h, int d){
    this(w, h, w / d, h / d);
  }

  Grid(int w, int h, int r, int c){
    this.GRID = new ArrayList<ArrayList>();

    this.W = w;
    this.H = h;
    this.R = r;
    this.C = c;
  }

  public void Create(){
    ArrayList<Boolean> ROW = new ArrayList<Boolean>();
    for(int i = 0; i < this.C; i++){
      ROW = new ArrayList<Boolean>();
      for(int j = 0; i < this.R; j++) {
        this.GRID.add(ROW);
      }
    }
  }

  public void setCell(int x, int y, boolean s){
    this.GRID.get(y).get(x).set(x, s);
  }

  public void getCell(int x, int y){
    System.out.println(this.GRID.get(y).get(x));
  }
}

class Draw extends Canvas{
  public static Grid g;

  public static void main(String arg[]) {
      JFrame frame = new JFrame("My Drawing");
      Canvas canvas = new Draw();
      canvas.setSize(400, 400);
      frame.add(canvas);
      frame.pack();
      frame.setVisible(true);

      Grid g = new Grid();
  }

  public void paint(Graphics g) {}

  public void drawGrid(Graphics g){

  }
}
